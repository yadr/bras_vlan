import re
import select
from ipaddress import IPv4Address
from logging import getLogger
from typing import Pattern, List, Tuple
from datetime import datetime

from paramiko import SSHClient, AutoAddPolicy, Transport, Channel
from vt100 import vt100


class Redback:
    more_re = re.compile(r'---\(more\)---')
    connect_timeout = 30  # todo make configurable?
    command_timeout = 20  # todo make configurable?

    def __init__(self, hostname: str, ip: IPv4Address, username: str, password: str, context: str,
                 link_group: str, link_group_type: str, pools: str, port: int = 22):  # todo kwargs/dictionary?..
        self.hostname = hostname
        self.ip = ip
        self.port = port
        self.username = username
        self.password = password

        self.context = context
        self.link_group = link_group
        self.link_group_type = link_group_type
        self.pools = pools

        self.prompt_re = re.compile(r'\[.+\].*#')  # todo: use hostname here?..
        self.prompt_ctx_re = re.compile(fr'\[{self.context}\].*#')
        self.prompt_cfg_re = re.compile(fr'\[{self.context}\].*\(config\)#')
        self.prompt_cfg_lg_re = re.compile(fr'\[{self.context}\].*\(config-link-group\)#')
        self.prompt_cfg_dot1q_re = re.compile(fr'\[{self.context}\].*\(config-dot1q-pvc\)#')
        self.prompt_cfg_ctx_re = re.compile(fr'\[{self.context}\].*\(config-ctx\)#')
        self.prompt_cfg_if_re = re.compile(fr'\[{self.context}\].*\(config-if\)#')

        self.ssh_client = SSHClient()
        self.ssh_client.set_missing_host_key_policy(AutoAddPolicy())

        self.history: List[Tuple[datetime, str]] = []

        self.logger = getLogger(f"{__name__}.{hostname}")
        self.shell_channel: Channel = None  # is None until you call connect()

    def connect(self) -> bool:
        try:
            self.ssh_client.connect(hostname=str(self.ip), port=self.port, username=self.username,
                                    password=self.password, timeout=self.connect_timeout)
            transport: Transport = self.ssh_client.get_transport()
            ssh_s: Channel = transport.open_session(timeout=self.connect_timeout)
            ssh_s.settimeout(self.command_timeout)
            ssh_s.set_combine_stderr(True)
            ssh_s.get_pty(term='vt100', width=254, height=10000)  # this height SHOULD be enough to fit everything.
            ssh_s.invoke_shell()
            self.shell_channel = ssh_s

            # wait for first prompt to appear: this is S L O W
            self.exec_await(cmd='', await_re=self.prompt_re)

            return True
        except:
            self.logger.exception("Failed to connect to bras and open shell channel")
            return False

    def exec_await(self, cmd: str, await_re: Pattern):
        if not self.shell_channel:
            raise ValueError("Call connect() before exec_and_await!")

        start_time = datetime.now()
        self.logger.debug(f"Executing '{cmd}', awaiting '{await_re}'")
        if self.shell_channel.closed:
            raise EOFError("shell channel closed!")

        self.shell_channel.sendall(f"{cmd}\r")
        stdout = ""

        while not re.search(await_re, stdout):
            if (datetime.now() - start_time).seconds >= self.command_timeout:  # todo external timeout?..
                raise TimeoutError(f"Timeout while waiting for '{await_re}' after '{cmd}'!")

            if self.shell_channel.recv_ready():
                readq, _, _ = select.select([self.shell_channel], [], [], 1)
                for c in readq:
                    if c.recv_ready():
                        stdout += self.shell_channel.recv(len(c.in_buffer)).decode()
            elif re.search(self.more_re, stdout):
                self.shell_channel.sendall(' ')

        ln_count = stdout.count('\r\n')
        height = ln_count + 1 if ln_count > 0 else 1

        zz = vt100(height, 254)
        zz.process(stdout)

        self.history.append((datetime.now(), zz.window_contents_formatted()))

        return zz.window_contents_formatted()

    def get_readable_history(self) -> str:
        result = ""
        for time, data in self.history:
            result += f'{time}: {data}'
        return result
