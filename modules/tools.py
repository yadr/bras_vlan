from telnetlib import Telnet
from typing import Dict
import logging
import socket
import re
from typing import Pattern
from ipaddress import IPv4Interface, AddressValueError, NetmaskValueError


def validate_config(config: Dict) -> bool:
    logger = logging.getLogger(__name__)
    result = True
    req_keys = {'data_host', 'data_port', 'redbacks'}

    keys = set(config.keys())
    missing = req_keys - keys
    extra = keys - req_keys

    if missing:
        logger.critical(f"top-level missing keys '{missing}'")
        result = False

    if extra:
        logger.warning(f"top-level extra keys '{extra}' found!. "
                       f"They will be ignored. ")

    req_bras_keys = {'address', 'login', 'pass', 'context', 'link_group', 'link_group_type', 'pools'}

    if isinstance(config, dict) and 'redbacks' in config:
        brases = config['redbacks']
        for bras in brases:
            bras_keys = set(brases[bras].keys())

            missing_keys = req_bras_keys - bras_keys
            extra_keys = bras_keys - req_bras_keys

            if missing_keys:
                logger.critical(f"missing keys '{missing_keys}' for bras '{bras}' ")
                result = False

            if extra_keys:
                logger.warning(f"extra keys '{extra_keys}' found for bras '{bras}'."
                               f" They will be ignored. ")

    else:
        logger.critical("config is not a dictionary!")
        result = False

    return result


def get_users(host: str, port: str, pools: str):
    logger = logging.getLogger(__name__)
    users = {}
    with Telnet(host=host, port=port, timeout=10) as tn:
        tn.write(pools.encode('ascii'))
        tn.get_socket().shutdown(socket.SHUT_WR)  # sends EOF, as actual telnet does when you do "echo | telnet"
        result = tn.read_all().decode('ascii')

    for line in result.split('\n'):
        if not line or line.startswith('endoflist_'):
            continue

        try:
            temp = line.split(' ')
            vlan_id = int(temp[0])
            if not 0 < vlan_id < 4095:
                logger.warning(f"vlan '{vlan_id}': out of 1<->4094 range!")
                continue

            iface = IPv4Interface(f"{temp[1]}/{temp[2]}")
            if not iface.is_global:
                logger.warning(f"Skipping interface '{iface}': not globally routable!")
                continue

            if iface.network.prefixlen == 32:
                logger.warning(f"Skipping interface '{iface}': /32 network!")
                continue

            users[vlan_id] = {'iface': iface}

        except (AddressValueError, NetmaskValueError):
            logger.warning(f"Unable to parse ipif on line '{line}'")
        except (IndexError, ValueError):
            logger.exception(f"Unable to parse line '{line}'")
    return users


def parse_vlans(stdout: str, vlan_re: Pattern):
    logger = logging.getLogger(__name__)
    vlans = {}
    for line in stdout.split('\n'):
        matches = re.match(vlan_re, line)
        if matches:
            vlan_id = int(matches.group('vlanid'))
            if not 0 < vlan_id < 4095:
                logger.warning(f"vlan '{vlan_id}': out of 1<->4094 range!")
                continue

            state = matches.group('state')
            descr = matches.group('descr')

            vlans[vlan_id] = {'state': state, 'descr': descr}
            logger.debug(f"found vlan, id: {vlan_id}, state: {state}, descr: {descr}")

    return vlans


def parse_interfaces(stdout: str, if_re: Pattern):
    logger = logging.getLogger(__name__)
    interfaces = {}
    for line in stdout.split('\n'):
        match = re.match(if_re, line)
        if match:
            ifname = match.group('ifname')
            ip = IPv4Interface(match.group('ip'))
            mtu = int(match.group('mtu'))
            state = match.group('state')
            bindings = match.group('bindings')
            interfaces[ip] = {"ifname": ifname, "ip": ip, "mtu": mtu, "state": state, "bindings": bindings}
            logger.debug(
                f"Found interface: name: {ifname}, ip: {ip}, mtu: {mtu}, state: {state}, bindings: {bindings}")
    return interfaces


class ExitCodes:
    config_read_fail = 1
    ssh_shell_fail = 2
    ssh_cmd_fail = 3
    mon_fail = 4
    no_oneshot_bras = 5


class Static:
    # line structure: ifname ip mtu state bindings
    if_re = re.compile(
        r'^(?P<ifname>.*)\s+(?P<ip>\d+\.\d+\.\d+\.\d+/\d{1,2})\s+(?P<mtu>\d+)\s+(?P<state>Bound|Unbound|Up)\s+(?P<bindings>.*)$')

    # line structure: lg vlan profile state encaps binding
    # profile may be empty
    # state SHOULD be (Up|Down|Shut), but just in case i match everything possible.
    vlan_re = re.compile(
        r'^(?P<lgid>.*)\s+(?P<vlanid>\d+){1,4}\s+(?P<profile>.*)\s+(?P<state>[a-zA-Z]+)\s+dot1q\s+(?P<descr>.*) \[.*\]')
