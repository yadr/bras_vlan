import argparse
import json
import logging
import re
from multiprocessing.dummy import Pool
from typing import Dict

from modules.Redback import Redback
from modules.tools import Static
from modules.tools import validate_config, get_users, ExitCodes, parse_interfaces, parse_vlans


def worker(config: Dict, rb: Redback, args):
    logger = logging.getLogger(f"MAIN.worker.{rb.hostname}")
    logger.info(f"Working on bras {rb.hostname}")

    try:
        logger.info("getting users from mon")
        users = get_users(host=config['data_host'], port=config['data_port'], pools=rb.pools)
        user_interfaces = set([i['iface'] for i in users.values()])
        logger.info(f"got {len(users)} users from mon")

        if len(users) == 0:
            raise ValueError('Got 0 users from mon, NOT deleting everything!')
    except:
        logger.exception(f"Failed to get users from '{config['data_host']}', port '{config['data_port']}'")
        return

    if not rb.connect():
        logger.critical("Failed to connect to bras!")
        return

    try:
        rb.exec_await('context {rb.context}', rb.prompt_ctx_re)
        out = rb.exec_await(f"show dot1q pvc link-group {rb.link_group} | include '#icvlan#'", rb.prompt_ctx_re)
        vlans = parse_vlans(stdout=out, vlan_re=Static.vlan_re) if not args.force else {}

        out = rb.exec_await("show ip interface brief | include '#icvlan#'", rb.prompt_re)
        interfaces = parse_interfaces(stdout=out, if_re=Static.if_re) if not args.force else {}

        interfaces_to_create = user_interfaces - set(interfaces.keys())
        logger.info(f"interfaces to create: {', '.join([str(i) for i in interfaces_to_create])}")
        if not args.noop and len(interfaces_to_create) > 0:
            rb.exec_await('config', rb.prompt_cfg_re)
            rb.exec_await(f'context {rb.context}', rb.prompt_cfg_ctx_re)

            for interface in interfaces_to_create:
                logger.info(f"Creating interface {interface}")
                rb.exec_await(f'interface #icvlan#{interface.network}#', rb.prompt_cfg_if_re)
                rb.exec_await(f'ip address {interface}', rb.prompt_cfg_if_re)
                rb.exec_await('ip arp timeout 850', rb.prompt_cfg_if_re)
                rb.exec_await('ip arp delete-expired', rb.prompt_cfg_if_re)

            rb.exec_await('commit', rb.prompt_cfg_if_re)
            rb.exec_await('end', rb.prompt_ctx_re)

        vlans_to_create = set(users.keys()) - set(vlans.keys())
        logger.info(f"vlans to create: {', '.join([str(i) for i in vlans_to_create])}")
        if not args.noop and len(vlans_to_create) > 0:
            rb.exec_await('config', rb.prompt_cfg_re)
            rb.exec_await(f'link-group {rb.link_group} {rb.link_group_type}', rb.prompt_cfg_lg_re)

            for vlan_id in vlans_to_create:
                logger.info(f"Creating vlan {vlan_id}")
                network = users[vlan_id]['iface'].network
                rb.exec_await(f'dot1q pvc {vlan_id} profile dot1q-stats', rb.prompt_cfg_dot1q_re)
                rb.exec_await(f'description #icvlan#{network}#', rb.prompt_cfg_dot1q_re)
                rb.exec_await(f'bind interface #icvlan#{network}# {rb.context}', rb.prompt_cfg_dot1q_re)
                rb.exec_await(f'flow apply ip profile FL-VLAN both', rb.prompt_cfg_dot1q_re)

            rb.exec_await('commit', rb.prompt_cfg_dot1q_re)
            rb.exec_await('end', rb.prompt_ctx_re)

        vlans_to_delete = set(vlans.keys()) - set(users.keys())
        logger.info(f"vlans to delete: {', '.join([str(i) for i in vlans_to_delete])}")
        if not args.noop and len(vlans_to_delete) > 0:
            rb.exec_await('config', rb.prompt_cfg_re)
            rb.exec_await(f'link-group {rb.link_group} {rb.link_group_type}', rb.prompt_cfg_lg_re)

            for vlan_id in vlans_to_delete:
                logger.info(f"Deleting vlan {vlan_id}")
                rb.exec_await(f"no dot1q pvc {vlan_id}", rb.prompt_cfg_lg_re)

            rb.exec_await('commit', rb.prompt_cfg_lg_re)
            rb.exec_await('end', rb.prompt_ctx_re)

        interfaces_to_delete = set(interfaces.keys()) - user_interfaces
        logger.info(f"interfaces to delete: {', '.join([str(i) for i in interfaces_to_delete])}")
        if not args.noop and len(interfaces_to_delete) > 0:
            rb.exec_await('config', rb.prompt_cfg_re)
            rb.exec_await('context {rb.context}', rb.prompt_cfg_ctx_re)

            for interface in interfaces_to_delete:
                logger.info(f"Deleting interface {interface}")
                rb.exec_await(f'no interface #icvlan#{interface.network}#', rb.prompt_cfg_ctx_re)

            rb.exec_await('commit', rb.prompt_cfg_ctx_re)
            rb.exec_await('end', rb.prompt_ctx_re)

        logger.info(f"End for bras {rb.hostname}")
        logger.debug(f"Stdout history was:\n{rb.get_readable_history()}")
    except:
        logger.exception("Fail! Read ze traceback!")
        try:
            rb.exec_await('abort', re.compile('.*'))
            rb.ssh_client.close()
        except:
            pass


def main():
    parser = argparse.ArgumentParser(description="Creates inet_kanal_vlan dot1q pvc and interfaces",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-d", "--debug", help="Turns on debug output", action="store_true")
    parser.add_argument("-p", "--paramiko_debug", help="Turns on paramiko debug output", action="store_true")
    parser.add_argument("-c", "--config", help="Config file location.", default="config.json")
    parser.add_argument("-o", "--oneshot", help="Hostname of bras to process and exit")
    parser.add_argument("-n", "--noop", help="Do not apply any vlan or ipif changes; only show what would be done",
                        action="store_true")
    parser.add_argument("-f", "--force", help="Ignore existing interfaces/vlans; Recreates vlans/interfaces",
                        action="store_true")  # todo better help
    args = parser.parse_args()

    loglevel = logging.DEBUG if args.debug else logging.INFO
    logger = logging.getLogger("MAIN")
    logger.setLevel(loglevel)
    logging.getLogger().setLevel(loglevel)  # set default loglevel

    sshloglevel = logging.DEBUG if args.paramiko_debug else logging.CRITICAL
    logging.getLogger("paramiko").setLevel(sshloglevel)

    logger.info(f"Starting")
    logger.debug(f"arguments: 'f{args}'")

    config = {}
    try:
        config = json.loads(''.join(open(args.config, 'r').readlines()))
        if not validate_config(config):
            raise ValueError("Failed to validate config!")

    except:
        logger.exception("Failed to read config!")
        exit(ExitCodes.config_read_fail)

    brases = {}
    for name, data in config['redbacks'].items():
        brases[name] = Redback(hostname=name, ip=data['address'], username=data['login'], password=data['pass'],
                               context=data['context'], link_group=data['link_group'], pools=data['pools'],
                               link_group_type=data['link_group_type'])

    if args.oneshot:
        if args.oneshot in brases:
            brases = {args.oneshot: brases[args.oneshot]}
            logger.info(f"Working ONLY on bras '{args.oneshot}'")
        else:
            logger.critical(f"oneshot bras '{args.oneshot}' not found in config! Exiting.")
            exit(ExitCodes.no_oneshot_bras)

    with Pool(processes=len(brases)) as pool:
        pool.starmap(worker, [(config, rb, args) for rb in brases.values()])


if __name__ == "__main__":
    logging.basicConfig(format='bras_vlan.py: [%(name)s] %(levelname)s, %(message)s')

    main()
